// function SetCookie(name, value, option = {}) {
//
//     options = {
//         path: '/',
//         expires: Date.now()
//     };
//     if (option.expires.toUTCString){
//         options.expires = options.expires.toUTCString();
//     }
//
//     let updatedCookie = encodeURIComponent(name) + '=' + encodeURIComponent(value);
//
//     for (let optionKey in option){
//         updatedCookie += '; ' + optionKey;
//         let optionValue = option[optionKey];
//         if(optionValue !== true){
//             updatedCookie += '=' + optionValue;
//         }
//     }
//     document.cookie = updatedCookie;
// }

let contactButton = document.querySelector('.contact');

function setCookie(){
    document.cookie = 'experiment=novalue; max-age=-1';
    let cookies = document.cookie.split('; ');
    (cookies.includes('new-user=true')) ? document.cookie = "new-user=false; max-age=300" : document.cookie = "new-user=true; max-age=300";
}

contactButton.addEventListener('click', function(e){
    e.preventDefault();
    setCookie();
});